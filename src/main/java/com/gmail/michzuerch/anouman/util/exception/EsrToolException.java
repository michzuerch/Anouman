package com.gmail.michzuerch.anouman.util.exception;

public class EsrToolException extends Exception {
    private static final long serialVersionUID = 1701946751615123600L;

	public EsrToolException() {
    }

    public EsrToolException(String message) {
        super(message);
    }
}
