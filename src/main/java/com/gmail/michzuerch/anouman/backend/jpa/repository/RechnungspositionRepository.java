package com.gmail.michzuerch.anouman.backend.jpa.repository;

import com.gmail.michzuerch.anouman.backend.jpa.domain.Rechnungsposition;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RechnungspositionRepository extends JpaRepository<Rechnungsposition, Long> {
}
