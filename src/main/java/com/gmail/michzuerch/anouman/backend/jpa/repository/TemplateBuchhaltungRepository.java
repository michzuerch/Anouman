package com.gmail.michzuerch.anouman.backend.jpa.repository;

import com.gmail.michzuerch.anouman.backend.jpa.domain.TemplateBuchhaltung;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TemplateBuchhaltungRepository extends JpaRepository<TemplateBuchhaltung, Long> {
}
