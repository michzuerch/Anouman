package com.gmail.michzuerch.anouman.backend.jpa.repository;

import com.gmail.michzuerch.anouman.backend.jpa.domain.TemplateMehrwertsteuercode;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TemplateMehrwertsteuercodeRepository extends JpaRepository<TemplateMehrwertsteuercode, Long> {
}
