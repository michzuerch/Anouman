package com.gmail.michzuerch.anouman.backend.jpa.repository;

import com.gmail.michzuerch.anouman.backend.jpa.domain.EditorTest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EditorTestRepository extends JpaRepository<EditorTest, Long> {
}
