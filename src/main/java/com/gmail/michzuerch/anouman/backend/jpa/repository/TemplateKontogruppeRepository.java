package com.gmail.michzuerch.anouman.backend.jpa.repository;

import com.gmail.michzuerch.anouman.backend.jpa.domain.TemplateKontogruppe;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TemplateKontogruppeRepository extends JpaRepository<TemplateKontogruppe, Long> {
}
