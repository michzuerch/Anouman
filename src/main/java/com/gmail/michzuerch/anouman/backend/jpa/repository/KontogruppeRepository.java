package com.gmail.michzuerch.anouman.backend.jpa.repository;

import com.gmail.michzuerch.anouman.backend.jpa.domain.Kontogruppe;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KontogruppeRepository extends JpaRepository<Kontogruppe, Long> {
}
