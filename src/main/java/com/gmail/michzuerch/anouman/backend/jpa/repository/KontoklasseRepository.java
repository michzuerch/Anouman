package com.gmail.michzuerch.anouman.backend.jpa.repository;

import com.gmail.michzuerch.anouman.backend.jpa.domain.Kontoklasse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KontoklasseRepository extends JpaRepository<Kontoklasse, Long> {
}
