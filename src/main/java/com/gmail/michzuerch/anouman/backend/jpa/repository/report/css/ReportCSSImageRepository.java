package com.gmail.michzuerch.anouman.backend.jpa.repository.report.css;

import com.gmail.michzuerch.anouman.backend.jpa.domain.report.css.ReportCSSImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportCSSImageRepository extends JpaRepository<ReportCSSImage, Long> {
}
