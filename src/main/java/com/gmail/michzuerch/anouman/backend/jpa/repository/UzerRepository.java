package com.gmail.michzuerch.anouman.backend.jpa.repository;

import com.gmail.michzuerch.anouman.backend.jpa.domain.Uzer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UzerRepository extends JpaRepository<Uzer, Long> {
}
