package com.gmail.michzuerch.anouman.backend.jpa.repository.report.jasper;

import com.gmail.michzuerch.anouman.backend.jpa.domain.report.jasper.ReportJasperImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportJasperImageRepository extends JpaRepository<ReportJasperImage, Long> {
}
