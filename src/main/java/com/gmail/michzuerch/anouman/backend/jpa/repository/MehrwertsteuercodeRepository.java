package com.gmail.michzuerch.anouman.backend.jpa.repository;

import com.gmail.michzuerch.anouman.backend.jpa.domain.Mehrwertsteuercode;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MehrwertsteuercodeRepository extends JpaRepository<Mehrwertsteuercode, Long> {
}
