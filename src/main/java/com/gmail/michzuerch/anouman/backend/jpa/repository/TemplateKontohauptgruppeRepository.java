package com.gmail.michzuerch.anouman.backend.jpa.repository;

import com.gmail.michzuerch.anouman.backend.jpa.domain.TemplateKontohauptgruppe;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TemplateKontohauptgruppeRepository extends JpaRepository<TemplateKontohauptgruppe, Long> {
}
