Anouman
==============

Webapplikation für Wildfly 11.

Funktionsumfang
========

CRM, Adressen, Rechnungen, Artikelstamm, Buchhaltung, Reports


Technologien
-------------------------

- Wildfly 11.0
- Vaadin 8.3.1
- Postgresql
- FOP
- Jasperreport


Installation
-------------------------

Source ist auf github: https://github.com/michzuerch/Anouman.git

Das Buildsystem ist Maven. Zum Installieren mvn "wildfly:deploy" eingeben.

Um Unterordner "Scripts" sind die Shell-Scripts zum erstellen der Datasources.